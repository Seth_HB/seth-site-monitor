#!/bin/bash

#This is the Seth's Site Monitor engine.
#You don't have to edit this file, unless you want to change the config file path
#or the sethlib.shlib path
#By default, it's in the folder you are curentely in

p_ConfigFilePath="./SSM.conf"
source ./sethlib/sethlib.shlib
s_ssmVersion="0.2"

#########################
#	error code table	#
#########################


#Ici se trouve la liste des code d'erreurs, suivie de la fonction qui les prends en charges.
#Notez que dans la plupart des cas, il s'agit simplement de renvoyer le code d'erreur, 
#accompagné d'un message

# 0 = no error
# 140 = config file not found
# 141 = Missing value in config file
# 142 = site config folder not found
# 143 = TSV output folder not found
# 144 = HTML output folder not found
# 145 = TSV folder not writable
# 146 = HTML folder not writable
# 147 = Missing value in a site config file
# 148 = Missing dependency
# 149 = Missing function
# 150 = f_getIpFromaddress malfunction
# 151 = f_testPingTime malfunction
# 255 = Unknown error

function f_ErrorHandler() {

	test $1 = 140 && { echo "Config file not found"; exit 140; }
	
	test $1 = 141 && { echo "Missing value in config file : $i"; exit 141; }
	
	test $1 = 142 && { echo "Site config folder not found"; exit 142; }
	
	test $1 = 143 && { echo "TSV output folder not found"; exit 143; }
	
	test $1 = 144 && { echo "HTML output folder not found"; exit 144; }

	test $1 = 145 && { echo "TSV output folder not writable"; exit 145; }

	test $1 = 146 && { echo "HTML output folder not writable"; exit 146; }
	
	test $1 = 147 && { echo "Error : Missing value in $i"; return; }
		
	test $1 = 148 && { echo "Error : Missing dependency : $i"; exit 148; }
		
	test $1 = 149 && { echo "Error : Missing function : $i. Please check sethlib.shlib"; exit 149; }
	
	test $1 = 150 && { echo "Error : f_getIpFromaddress malfunction when trying $i. Please check sethlib.shlib"; exit 150; }
	
	test $1 = 151 && { echo "Error : f_testPingTime. Please check sethlib.shlib"; exit 151; }
	
	test $1 = 152 && { echo "Memory output folder not found"; s_enableDiscordReport=false ; return ; }
	
	test $1 = 153 && { echo "Memory output folder not writable"; s_enableDiscordReport=false ; return ;}

	echo "Unknown error"; exit 255;
	
}

#########################
#	dependence check	#
#########################


#Rien de compliqué, a_dependencyArray contien la liste des dependences utilisé par SOT
#elle sont toute testées pour s'assuré qu'on a bien tous a disposition
#dans le cas contraire f_ErrorHandler s'occupera de le reporté a l'utilisateur
a_dependencyArray=(ping curl sed)

for i in ${a_dependencyArray[*]}
do

	command -v $i >/dev/null 2>&1 || f_ErrorHandler 148

done

#sethlib.shlib contient des fonctions generiques utilisées dans SOT. 
#a_functionDependencyArray contien la liste de ses fonctions.
#On en test l'existence une à une au cas où certaine serait manquante
#a cause d'un propleme de version ou autre
#Note : On verifie uniquement leurs existences, pas leurs fonctionement
a_functionDependencyArray=(f_ReadConfigLine f_getIpFromaddress f_testPingTime f_testHttpConnect)

for i in ${a_functionDependencyArray[*]}
do

	type $i > /dev/null 2>&1
	test $? != 0 && f_ErrorHandler 149

done



#########################
#	engine autoconf		#
#########################

#Ok, es que le fichier de conf existe ?

test -f $p_ConfigFilePath || f_ErrorHandler 140

#Le fichier existe, on defini un array avec toutes les options qui nous interesse
#puis on extrait et test toutes ses options.

a_ConfigOptionArray=(p_siteConfigFolder p_htmlOutputFolder p_tsvOutputFolder s_enableDiscordReport s_discordWebhookKey s_discordBotAvatar s_discordBotName p_memoryFolder s_siteLostMsg s_siteUpAgaintMsg s_siteIpLostPingMsg s_siteIpPingAgaintMsg s_siteIpLostAddressMsg s_siteAddressPingAgaintMsg)

for i in ${a_ConfigOptionArray[*]}
do

	#generer dynamiquement un nom de variable se fait ainsi
	#notez que pour y faire reference immediatement, il faut une reference indirect
    declare "$i=$(f_ReadConfigLine $p_ConfigFilePath $i)"
    ref="$i" 
    test "${!ref}" = "NOT_FOUND" && f_ErrorHandler 141

done


#On verifie que les dossiers appelés existe
test -d $p_siteConfigFolder || f_ErrorHandler 142
test -d $p_tsvOutputFolder || f_ErrorHandler 143

#p_htmlOutputFolder est un peu special, dans le sens ou il peut être desactivé avec "none"
test $p_htmlOutputFolder = "none" || { test -d $p_htmlOutputFolder || f_ErrorHandler 144; }

#Bien, mais es que je peux ecrire là où j'ai besoin ?
test -w $p_tsvOutputFolder || f_ErrorHandler 145
test $p_htmlOutputFolder = "none" || { test -w $p_htmlOutputFolder || f_ErrorHandler 146; }

#Maintenant, on s'occupe du dossier memory si discord si il est activé ($s_enableDiscordReport à "true")
#Si le dossier n'existe pas ou n'est pas inscriptible, 
if [ $s_enableDiscordReport = "true" ]
then

	#Pas question de spammer à chaque fois, il nous faut une mémoire. C'est dans le dossier pointé par p_memoryFolder
	#On verifie que le dossier appelé existe f_ErrorHandler le signal et remet s_enableDiscordReport à false
	test -d $p_memoryFolder || f_ErrorHandler 152

	#Bien, mais es que je peux ecrire là où j'ai besoin ?
	test -w $p_memoryFolder || f_ErrorHandler 153

fi


#bien, vu que le dossier de conf pour les sites existe, il doit bien contenir quelque chose non ?
#tachons d'extraire le contenu des fichier de conf présents
#nota : peu importe comment ils sont nommés, ils doivent juste ce finir par ".conf"
#		Si un fichier n'est pas valide (valeur manquante), f_ErrorHandler le signalera
#		mais ne bloque pas le fonctionement de SOT, le fichier est juste ignoré

a_siteConfigFile=($(ls $p_siteConfigFolder*.conf))

#C'est partie pour generer l'array qui va contenir tous les sites à tester
#i_siteCounter contien l'index du site en cour de traitement
i_LoopCounter=0
for i in ${a_siteConfigFile[*]}
do

	#extraction des informations du fichier
    s_siteName="$(f_ReadConfigLine $i s_SiteName)"
	s_siteaddress="$(f_ReadConfigLine $i s_siteaddress)"
	fl_maxLoadingTime="$(f_ReadConfigLine $i fl_maxLoadingTime)"
	s_siteProtocol="$(f_ReadConfigLine $i s_siteProtocol)"
	

    if test $s_siteName = "NOT_FOUND" || test $s_siteaddress = "NOT_FOUND" || test $fl_maxLoadingTime = "NOT_FOUND" || test $s_siteProtocol = "NOT_FOUND"
    then
		
		#il manque quelque-chose, f_ErrorHandler est appeler et le fichier est ignoré
		f_ErrorHandler 147
		
	else
		
		#on a tout ? let's go !
		
		#Bash ne suporte pas les tableaux imbriqués, mais on s'en sort avec plusieurs tableaux
		a_sitesListName[$i_LoopCounter]=$s_siteName
		a_sitesListaddress[$i_LoopCounter]=$s_siteaddress
		a_sitesListmaxLoadingTime[$i_LoopCounter]=$fl_maxLoadingTime
		a_sitesListProtocol[$i_LoopCounter]=$s_siteProtocol
		
		#il faut incrementer le compteur
		i_LoopCounter=$((i_LoopCounter + 1))
	fi	

done

### FIN DE L'AUTO-CONFIG ###

#####################
#	Site testing	#
#####################

#Cette partie contien les tests eux-mêmes sur les sites. 
#Notez que le timetamp qui sera retenu sera celui du moment ou l'on arrive ici,
#pas celui de l'execution des tests. Ça facilite les croisement.
#On prend aussi les valeur de date pour les dossiers d'exportation
#Ainsi qu'une version avec la date et l'heure complete, au format humainement lisible
#pour integration dans les fichier eux-même
i_testTimeStamp=$(date +%s)
s_dateStringForFolder=$(date +%Y-%m)
s_dateStringForFile=$(date +"%a %d %b %Y - %T")

#ok, on a la liste de se qu'on veut tester, on va commencer par voir si la resolion DNS marche
#pour ça on va tester tout le contenu de a_sitesListaddress, si une IP est trouvé on la placera dans a_sitesListIp au même index.
#Dans le cas contraire c'est "Not_Found" qui est stoqué.
#Noter qu'on execute se test qu'en IPv4.

i_LoopCounter=0
for i in ${a_sitesListaddress[*]}
do

	f_getIpFromaddress $i
	
	#si la valeur est vide, quelque chose ne va pas ...
	test $s_result = empty && f_ErrorHandler 150
	
	
	a_sitesListIp[$i_LoopCounter]=$s_result

	#il faut incrementer le compteur
	i_LoopCounter=$((i_LoopCounter + 1))
	

done

#Ok, on a la listes des IP, on va essayer de ping ses IP pour voir si le serveur répond
#Si il répond, on stock le temps moyen de réponse dans a_sitesServerAvgPing et "ok" dans a_sitesServerPingStatus à l'index qui correspond
#Si un code d'erreur 1 se produit, le temps moyen de réponse dans a_sitesServerAvgPing et "Some_Lost" dans a_sitesServerPingStatus à l'index qui correspond
#Si un code d'erreur 2 se produit, on met "0" dans a_sitesServerAvgPing et "ERROR" dans a_sitesServerPingStatus à l'index qui correspond
#important : on fait ici le ping sur l'IP, pas l'addresse. Ça c'est pour juste après
i_LoopCounter=0
for i in ${a_sitesListName[*]}
do

	#Si a_sitesListIp[$i_LoopCounter] contien "Not_Found", c'est que la resolution DNS
	#ne fonctionne pas. Dans ce cas on passe.
	if [ ${a_sitesListIp[$i_LoopCounter]} != "Not_Found" ]
	then
	
		f_testPingTime ${a_sitesListIp[$i_LoopCounter]}
		s_error_code=$?
	
		test $s_error_code = 0 && { a_sitesServerAvgPing[$i_LoopCounter]=$s_result ; a_sitesServerPingStatus[$i_LoopCounter]="OK" ;}
		test $s_error_code = 1 && { a_sitesServerAvgPing[$i_LoopCounter]=$s_result ; a_sitesServerPingStatus[$i_LoopCounter]="Some_Lost" ;}
		test $s_error_code = 2 && { a_sitesServerAvgPing[$i_LoopCounter]=0 ; a_sitesServerPingStatus[$i_LoopCounter]="ERROR" ;}
		
		if test $s_error_code != 0 && test $s_error_code != 1 && test $s_error_code != 2
		then
		
			#Quelque chose ne va pas avec f_testPingTime si on arrive là

			f_ErrorHandler 151
		
		fi
		
	fi
	#il faut incrementer le compteur
	i_LoopCounter=$((i_LoopCounter + 1))
	
done

#On prend les même et on recommence, mais cette fois c'est un ping sur l'addresse du site, pas sur l'IP du serveur
#Si ça répond, on stock le temps moyen de réponse dans a_sitesAddressAvgPing et "ok" dans a_sitesAddressPingStatus à l'index qui correspond
#Si un code d'erreur 1 se produit, le temps moyen de réponse dans a_sitesAddressAvgPing et "Some_Lost" dans a_sitesAddressPingStatus à l'index qui correspond
#Si un code d'erreur 2 se produit, on met "0" dans a_sitesAddressAvgPing et "ERROR" dans a_sitesAddressPingStatus à l'index qui correspond
i_LoopCounter=0
for i in ${a_sitesListName[*]}
do

	#Si a_sitesListIp[$i_LoopCounter] contien "Not_Found", c'est que la resolution DNS
	#ne fonctionne pas. Dans ce cas on passe.
	if [ ${a_sitesListIp[$i_LoopCounter]} != "Not_Found" ]
	then
	
		f_testPingTime ${a_sitesListaddress[$i_LoopCounter]}
		s_error_code=$?
	
		test $s_error_code = 0 && { a_sitesAddressAvgPing[$i_LoopCounter]=$s_result ; a_sitesAddressPingStatus[$i_LoopCounter]="OK" ;}
		test $s_error_code = 1 && { a_sitesAddressAvgPing[$i_LoopCounter]=$s_result ; a_sitesAddressPingStatus[$i_LoopCounter]="Some_Lost" ;}
		test $s_error_code = 2 && { a_sitesAddressAvgPing[$i_LoopCounter]=0 ; a_sitesAddressPingStatus[$i_LoopCounter]="ERROR" ;}
		
		if test $s_error_code != 0 && test $s_error_code != 1 && test $s_error_code != 2
		then
		
			#Quelque chose ne va pas avec f_testPingTime si on arrive là
			f_ErrorHandler 151
		
		fi
		
	fi
	#il faut incrementer le compteur
	i_LoopCounter=$((i_LoopCounter + 1))
	
done

#On va maintenant tenter de contacter les serveurs pour charger une page web.
#
i_LoopCounter=0
for i in ${a_sitesListName[*]}
do

	#Si a_sitesListIp[$i_LoopCounter] contien "Not_Found", c'est que la resolution DNS
	#ne fonctionne pas. Dans ce cas on passe.
	if [ ${a_sitesListIp[$i_LoopCounter]} != "Not_Found" ]
	then
	
	
		s_addressToTest="${a_sitesListProtocol[$i_LoopCounter]}://${a_sitesListaddress[$i_LoopCounter]}"
		f_testHttpConnect $s_addressToTest ${a_sitesListmaxLoadingTime[$i_LoopCounter]}
		s_error_code=$?
	
		if [ $s_error_code = 0 ]
		then
		
			#Ici, l'execution de f_testHttpConnect c'est passé sans probleme
			#a_result contien donc un array
			#on commence par mettre "OK" dans a_sitesListHttpStatus a l'index corespondant
			#puis on remplis les arrays
			
			a_sitesListHttpStatus[$i_LoopCounter]="OK"
			a_sitesListHttpCode[$i_LoopCounter]=${a_result[0]}
			a_sitesListHttpLocalIp[$i_LoopCounter]=${a_result[1]}
			a_sitesListHttpRemoteIp[$i_LoopCounter]=${a_result[2]}
			a_sitesListHttpDnsTime[$i_LoopCounter]=${a_result[3]}
			a_sitesListHttpTotalTime[$i_LoopCounter]=${a_result[4]}
			a_sitesListHttpSize[$i_LoopCounter]=${a_result[5]}
			a_sitesListHttpSpeed[$i_LoopCounter]=${a_result[6]}
			
		else	
		
			#Ici, quelque chose c'est mal passé a l'execution de f_testHttpConnect
			#On copie le contenu de a_result (qui contien juste le message d'erreur) dans a_sitesListHttpStatus
			#On met "ERROR" dans les autres arrays
		
			a_sitesListHttpStatus[$i_LoopCounter]=$a_result

			a_sitesListHttpCode[$i_LoopCounter]="ERROR"
			a_sitesListHttpLocalIp[$i_LoopCounter]="ERROR"
			a_sitesListHttpRemoteIp[$i_LoopCounter]="ERROR"
			a_sitesListHttpDnsTime[$i_LoopCounter]="ERROR"
			a_sitesListHttpTotalTime[$i_LoopCounter]="ERROR"
			a_sitesListHttpSize[$i_LoopCounter]="ERROR"
			a_sitesListHttpSpeed[$i_LoopCounter]="ERROR"

		fi
				
	fi
	#il faut incrementer le compteur
	i_LoopCounter=$((i_LoopCounter + 1))
	
done

### FIN DES TESTS ###

#####################
#	data export		#
#####################

#Tous les tests sont fait, il est temps d'en faire quelque chose

#On commence par l'export TSV
#On verifie que le dossier de destination existe pour cette année et mois. Sinon on le crée
test -d $p_tsvOutputFolder/$s_dateStringForFolder || mkdir $p_tsvOutputFolder/$s_dateStringForFolder

#idem, mais pour les fichiers. Aditionellement, on y ajoute l'en-tête explicatif si les fichiers n'existe pas
s_tsvFileHeader="Timestamp\tDate\tSSM version\tSeth's Lib version\tlocal host name\tSite\tAddress\tResolved server IP\tPing on IP status\tAverage IP ping time\tPing on address status\tAverage address ping time\tHTTP protocol\tHTTP connect status\tHTTP code\tHTTP local IP\tHTTP remote IP\tHTTP DNS querry time (s)\tHTTP total opperation time (s)\tHTTP download size (bytes)\tHTTP download speed (bytes/s)"
test -f $p_tsvOutputFolder/$s_dateStringForFolder/$s_dateStringForFolder.all.tsv || echo -e $s_tsvFileHeader > $p_tsvOutputFolder/$s_dateStringForFolder/$s_dateStringForFolder.all.tsv
test -f $p_tsvOutputFolder/$s_dateStringForFolder/$s_dateStringForFolder.error.tsv || echo -e $s_tsvFileHeader > $p_tsvOutputFolder/$s_dateStringForFolder/$s_dateStringForFolder.error.tsv

#Bien, maintenant on insére les données dans le fichier .all.tsv et .error.tsv

i_LoopCounter=0
for i in ${a_sitesListName[*]}
do

	#Si a_sitesListHttpStatus a cet index contien "OK", c'est que le test HTTP fonctionne, dans le cas contraire
	#pas la peine de remplir le fichier avec "ERROR", a_sitesListHttpStatus suffira
	if [ "${a_sitesListHttpStatus[$i_LoopCounter]}" = "OK" ]
	then
	
		echo -e "$i_testTimeStamp\t$s_dateStringForFile\t$s_ssmVersion\t$s_sethLibVersion\t$HOSTNAME\t${a_sitesListName[i_LoopCounter]}\t${a_sitesListaddress[i_LoopCounter]}\t${a_sitesListIp[$i_LoopCounter]}\t${a_sitesServerPingStatus[$i_LoopCounter]}\t${a_sitesServerAvgPing[$i_LoopCounter]}\t${a_sitesAddressPingStatus[$i_LoopCounter]}\t${a_sitesAddressAvgPing[$i_LoopCounter]}\t${a_sitesListProtocol[$i_LoopCounter]}\t${a_sitesListHttpStatus[$i_LoopCounter]}\t${a_sitesListHttpCode[$i_LoopCounter]}\t${a_sitesListHttpLocalIp[$i_LoopCounter]}\t${a_sitesListHttpRemoteIp[$i_LoopCounter]}\t${a_sitesListHttpDnsTime[$i_LoopCounter]}\t${a_sitesListHttpTotalTime[$i_LoopCounter]}\t${a_sitesListHttpSize[$i_LoopCounter]}\t${a_sitesListHttpSpeed[$i_LoopCounter]}" >> $p_tsvOutputFolder/$s_dateStringForFolder/$s_dateStringForFolder.all.tsv
	
		#Si les ping ne renvois pas OK, c'est une erreur a mettre dans le log
		test ${a_sitesServerPingStatus[$i_LoopCounter]} != "OK" && echo -e "$i_testTimeStamp\t$s_dateStringForFile\t$s_ssmVersion\t$s_sethLibVersion\t$HOSTNAME\t${a_sitesListName[i_LoopCounter]}\t${a_sitesListaddress[i_LoopCounter]}\t${a_sitesListIp[$i_LoopCounter]}\t${a_sitesServerPingStatus[$i_LoopCounter]}\t${a_sitesServerAvgPing[$i_LoopCounter]}\t${a_sitesAddressPingStatus[$i_LoopCounter]}\t${a_sitesAddressAvgPing[$i_LoopCounter]}\t${a_sitesListProtocol[$i_LoopCounter]}\t${a_sitesListHttpStatus[$i_LoopCounter]}\t${a_sitesListHttpCode[$i_LoopCounter]}\t${a_sitesListHttpLocalIp[$i_LoopCounter]}\t${a_sitesListHttpRemoteIp[$i_LoopCounter]}\t${a_sitesListHttpDnsTime[$i_LoopCounter]}\t${a_sitesListHttpTotalTime[$i_LoopCounter]}\t${a_sitesListHttpSize[$i_LoopCounter]}\t${a_sitesListHttpSpeed[$i_LoopCounter]}" >> $p_tsvOutputFolder/$s_dateStringForFolder/$s_dateStringForFolder.error.tsv
		test ${a_sitesAddressPingStatus[$i_LoopCounter]} != "OK" && echo -e "$i_testTimeStamp\t$s_dateStringForFile\t$s_ssmVersion\t$s_sethLibVersion\t$HOSTNAME\t${a_sitesListName[i_LoopCounter]}\t${a_sitesListaddress[i_LoopCounter]}\t${a_sitesListIp[$i_LoopCounter]}\t${a_sitesServerPingStatus[$i_LoopCounter]}\t${a_sitesServerAvgPing[$i_LoopCounter]}\t${a_sitesAddressPingStatus[$i_LoopCounter]}\t${a_sitesAddressAvgPing[$i_LoopCounter]}\t${a_sitesListProtocol[$i_LoopCounter]}\t${a_sitesListHttpStatus[$i_LoopCounter]}\t${a_sitesListHttpCode[$i_LoopCounter]}\t${a_sitesListHttpLocalIp[$i_LoopCounter]}\t${a_sitesListHttpRemoteIp[$i_LoopCounter]}\t${a_sitesListHttpDnsTime[$i_LoopCounter]}\$t{a_sitesListHttpTotalTime[$i_LoopCounter]}\t${a_sitesListHttpSize[$i_LoopCounter]}\t${a_sitesListHttpSpeed[$i_LoopCounter]}" >> $p_tsvOutputFolder/$s_dateStringForFolder/$s_dateStringForFolder.error.tsv

	
	else
	
		echo -e "$i_testTimeStamp\t$s_dateStringForFile\t$s_ssmVersion\t$s_sethLibVersion\t$HOSTNAME\t${a_sitesListName[i_LoopCounter]}\t${a_sitesListaddress[i_LoopCounter]}\t${a_sitesListIp[$i_LoopCounter]}\t${a_sitesServerPingStatus[$i_LoopCounter]}\t${a_sitesServerAvgPing[$i_LoopCounter]}\t${a_sitesAddressPingStatus[$i_LoopCounter]}\t${a_sitesAddressAvgPing[$i_LoopCounter]}\t${a_sitesListProtocol[$i_LoopCounter]}\t${a_sitesListHttpStatus[$i_LoopCounter]}" >> $p_tsvOutputFolder/$s_dateStringForFolder/$s_dateStringForFolder.all.tsv
		echo -e "$i_testTimeStamp\t$s_dateStringForFile\t$s_ssmVersion\t$s_sethLibVersion\t$HOSTNAME\t${a_sitesListName[i_LoopCounter]}\t${a_sitesListaddress[i_LoopCounter]}\t${a_sitesListIp[$i_LoopCounter]}\t${a_sitesServerPingStatus[$i_LoopCounter]}\t${a_sitesServerAvgPing[$i_LoopCounter]}\t${a_sitesAddressPingStatus[$i_LoopCounter]}\t${a_sitesAddressAvgPing[$i_LoopCounter]}\t${a_sitesListProtocol[$i_LoopCounter]}\t${a_sitesListHttpStatus[$i_LoopCounter]}" >> $p_tsvOutputFolder/$s_dateStringForFolder/$s_dateStringForFolder.error.tsv

	fi

	#il faut incrementer le compteur
	i_LoopCounter=$((i_LoopCounter + 1))
		
done

#Maintenant, on s'occupe du HTML si il est activé ($p_htmlOutputFolder différent de "none")
if [ $p_htmlOutputFolder != "none" ]
then

	#On verifie que le dossier de destination existe pour cette année et mois. Sinon on le crée
	test -d $p_htmlOutputFolder/$s_dateStringForFolder || mkdir $p_htmlOutputFolder/$s_dateStringForFolder
	
	#Ceci est le contenu de "index.html", le fichier qui permet d'acceder aux logs
	
	
	#On verifie que le "index.html" existe, si non on le crée et on met en place son contenu
	test -f $p_htmlOutputFolder/index.html || { cat $p_siteConfigFolder/HTML/index_header.html > $p_htmlOutputFolder/index.html ; cat $p_siteConfigFolder/HTML/index_infobar.html >> $p_htmlOutputFolder/index.html ; cat $p_siteConfigFolder/HTML/index_content.html >> $p_htmlOutputFolder/index.html ; cat $p_siteConfigFolder/HTML/index_footer.html >> $p_htmlOutputFolder/index.html ;}
	
	#idem, mais pour le contenu lui-même. Le fichier est géré a part pour être plus aisée a manipuler et permetre de naviger sans recharger l'intégralité de la page
	test -f $p_htmlOutputFolder/root_index_content.html || { cat $p_siteConfigFolder/HTML/root_data.html > $p_htmlOutputFolder/root_index_content.html ;}
	
	#On ajoute la ligne permetant d'acceder au log du mois si elle n'existe pas. Notez qu'il faut echaper tout les / pour sed (ajouter \ devant)
	grep -Fq "<tr><td>$s_dateStringForFolder</td><td>$s_ssmVersion</td><td>full</td><td><a href=\"./$s_dateStringForFolder/all.html\">link</a></td></tr>" $p_htmlOutputFolder/root_index_content.html
	test $? = 1 && sed -i "$(( $( wc -l < $p_htmlOutputFolder/root_index_content.html ) -2 ))s/$/\n<tr><td>$s_dateStringForFolder<\/td><td>$s_ssmVersion<\/td><td>full<\/td><td><a href=\".\/$s_dateStringForFolder\/all.html\">link<\/a><\/td><\/tr>/" $p_htmlOutputFolder/root_index_content.html
	
	#Idem, on ajoute la ligne permetant d'acceder au log contenant que les erreurs du mois si elle n'existe pas.
	grep -Fq "<tr><td>$s_dateStringForFolder</td><td>$s_ssmVersion</td><td>error</td><td><a href=\"./$s_dateStringForFolder/error.html\">link</a></td></tr>" $p_htmlOutputFolder/root_index_content.html
	test $? = 1 && sed -i "$(( $( wc -l < $p_htmlOutputFolder/root_index_content.html ) -2 ))s/$/\n<tr><td>$s_dateStringForFolder<\/td><td>$s_ssmVersion<\/td><td>error<\/td><td><a href=\".\/$s_dateStringForFolder\/error.html\">link<\/a><\/td><\/tr>/" $p_htmlOutputFolder/root_index_content.html
	
	#On verifie que les pages pour le mois en cour existe, sinon on les crée avec leurs en-têtes
	test -f $p_htmlOutputFolder/$s_dateStringForFolder/all.html || { cat $p_siteConfigFolder/HTML/month_data.html > $p_htmlOutputFolder/$s_dateStringForFolder/all.html ;}
	test -f $p_htmlOutputFolder/$s_dateStringForFolder/error.html || { cat $p_siteConfigFolder/HTML/month_data_error.html > $p_htmlOutputFolder/$s_dateStringForFolder/error.html ;}

	#Les fichier son en place, il est temps de les remplir
	i_LoopCounter=0
	s_sedString=empty
	for i in ${a_sitesListName[*]}
	do
	
		#Si a_sitesListHttpStatus a cet index contien "OK", c'est que le test HTTP fonctionne, dans le cas contraire
		#pas la peine de remplir le fichier avec "ERROR", a_sitesListHttpStatus suffira
		if [ "${a_sitesListHttpStatus[$i_LoopCounter]}" = "OK" ]
		then
		
			#La commande pour ajouter une ligne etant particulierement illisible, on passe par une variable. 
			#IMPORTANT : vous devez echaper tout les /, sinon sed ne va pas fonctionner correctement
			s_sedString="<tr><td>$i_testTimeStamp<\/td>\n"
			s_sedString=$s_sedString"<td>$s_dateStringForFile<\/td>\n"
			s_sedString=$s_sedString"<td>$s_ssmVersion<\/td>\n"
			s_sedString=$s_sedString"<td>$s_sethLibVersion<\/td>\n"
			s_sedString=$s_sedString"<td>$HOSTNAME<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListName[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td><a href=\"${a_sitesListProtocol[i_LoopCounter]}:\/\/${a_sitesListaddress[i_LoopCounter]}\"  target=\"_blank\" rel=\"noopener\">${a_sitesListaddress[i_LoopCounter]}<\/a><\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListIp[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesServerPingStatus[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesServerAvgPing[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesAddressPingStatus[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesAddressAvgPing[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListProtocol[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListHttpStatus[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListHttpCode[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListHttpLocalIp[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListHttpRemoteIp[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListHttpDnsTime[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListHttpTotalTime[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListHttpSize[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListHttpSpeed[i_LoopCounter]}<\/td><\/tr>\n"

			sed -i "$(( $( wc -l < $p_htmlOutputFolder/$s_dateStringForFolder/all.html ) -2 ))s/$/\n$s_sedString/" $p_htmlOutputFolder/$s_dateStringForFolder/all.html
			
			#Si les ping ne renvois pas OK, c'est une erreur a mettre dans le log
			test ${a_sitesServerPingStatus[$i_LoopCounter]} != "OK" && sed -i "$(( $( wc -l < $p_htmlOutputFolder/$s_dateStringForFolder/error.html ) -2 ))s/$/\n$s_sedString/" $p_htmlOutputFolder/$s_dateStringForFolder/error.html
			test ${a_sitesAddressPingStatus[$i_LoopCounter]} != "OK" && sed -i "$(( $( wc -l < $p_htmlOutputFolder/$s_dateStringForFolder/error.html ) -2 ))s/$/\n$s_sedString/" $p_htmlOutputFolder/$s_dateStringForFolder/error.html
		
		else
		
			#La commande pour ajouter une ligne etant particulierement illisible, on passe par une variable. 
			#IMPORTANT : vous devez echaper tout les /, sinon sed ne va pas fonctionner correctement
			s_sedString="<tr><td>$i_testTimeStamp<\/td>\n"
			s_sedString=$s_sedString"<td>$s_dateStringForFile<\/td>\n"
			s_sedString=$s_sedString"<td>$s_ssmVersion<\/td>\n"
			s_sedString=$s_sedString"<td>$s_sethLibVersion<\/td>\n"
			s_sedString=$s_sedString"<td>$HOSTNAME<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListName[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td><a href=\"${a_sitesListProtocol[i_LoopCounter]}:\/\/${a_sitesListaddress[i_LoopCounter]}\"  target=\"_blank\" rel=\"noopener\">${a_sitesListaddress[i_LoopCounter]}<\/a><\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListIp[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesServerPingStatus[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesServerAvgPing[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesAddressPingStatus[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesAddressAvgPing[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListProtocol[i_LoopCounter]}<\/td>\n"
			s_sedString=$s_sedString"<td>${a_sitesListHttpStatus[i_LoopCounter]}<\/td><\/tr>\n"

			sed -i "$(( $( wc -l < $p_htmlOutputFolder/$s_dateStringForFolder/all.html ) -2 ))s/$/\n$s_sedString/" $p_htmlOutputFolder/$s_dateStringForFolder/all.html
			sed -i "$(( $( wc -l < $p_htmlOutputFolder/$s_dateStringForFolder/error.html ) -2 ))s/$/\n$s_sedString/" $p_htmlOutputFolder/$s_dateStringForFolder/error.html
			
		fi
	
		#il faut incrementer le compteur
		i_LoopCounter=$((i_LoopCounter + 1))
			
	done

fi

#Maintenant, on s'occupe du discord si il est activé ($s_enableDiscordReport à "true")
if [ $s_enableDiscordReport = "true" ]
then

	#on va passer en revu toute les sites
	i_LoopCounter=0
	for i in ${a_sitesListName[*]}
	do

		#Si a_sitesListHttpStatus à cet index contien "OK", c'est que le test HTTP fonctionne, c'est le contraire qui nous interesse
		if [ "${a_sitesListHttpStatus[$i_LoopCounter]}" != "OK" ]
		then
		
			#Pas question de spammer tout les X minutes, ça va juste devenir illisible. 
			#On va utiliser le dossier p_memoryFolder pour savoir si l'annonce est deja faite
			test -f $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.http.memory || { echo -e "timestamp=$i_testTimeStamp" > $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.http.memory ; sendToDiscordChanel "$s_discordWebhookKey" "$s_siteLostMsg ${a_sitesListName[$i_LoopCounter]}" "$s_discordBotName" "$s_discordBotAvatar" ;}
		
		else
		
			#tout va bien, mais es que c'etait le cas précédement ?		
			if test -f $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.http.memory
			then
			
				#un fichier http.memory est trouver, on sort donc dans un downtime
				#il y a une annonce à faire, pour ça on a besoin de quelques info				
				i_downTimestamp=$(f_ReadConfigLine $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.http.memory timestamp)
				i_elapsedDownTime=$(( $i_testTimeStamp - $i_downTimestamp))
				s_elapsedDownTime=$(convertAndPrintSeconds i_elapsedDownTime)
				s_dateOfDownTime=$(date -d @$i_downTimestamp)
				
				#un peu de mise en forme
				s_msg="$s_siteUpAgaintMsg ${a_sitesListName[$i_LoopCounter]} ($s_dateOfDownTime : $s_elapsedDownTime )"

				#et on envois
				sendToDiscordChanel "$s_discordWebhookKey" "$s_msg" "$s_discordBotName" "$s_discordBotAvatar"
				
				#Et on supprime le fichier http.memory, on en a plus besoin
				rm $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.http.memory
				
			fi
		
		fi
		
		#et on efface l'evantuelle message
		s_msg=empty
		
		#On signal les problemes de ping sur IP maintenant. Notez qu'on fait pas de différence entre une perte partielle ou totale.
		#Pour ça, il faut voir dans les logs eux-même
		if [ "${a_sitesListHttpStatus[$i_LoopCounter]}" != "OK" ]
		then
		
			#Pas question de spammer tout les X minutes, ça va juste devenir illisible. 
			#On va utiliser le dossier p_memoryFolder pour savoir si l'annonce est deja faite
			test -f $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.ip.memory || { echo -e "timestamp=$i_testTimeStamp" > $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.ip.memory ; sendToDiscordChanel "$s_discordWebhookKey" "$s_siteIpLostPingMsg ${a_sitesListName[$i_LoopCounter]} (${a_sitesListIp[i_LoopCounter]})" "$s_discordBotName" "$s_discordBotAvatar" ;}
		
		else
		
			#tout va bien, mais es que c'etait le cas précédement ?		
			if test -f $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.ip.memory
			then
			
				#un fichier http.memory est trouver, on sort donc dans un downtime
				#il y a une annonce à faire, pour ça on a besoin de quelques info				
				i_downTimestamp=$(f_ReadConfigLine $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.ip.memory timestamp)
				i_elapsedDownTime=$(( $i_testTimeStamp - $i_downTimestamp))
				s_elapsedDownTime=$(convertAndPrintSeconds i_elapsedDownTime)
				s_dateOfDownTime=$(date -d @$i_downTimestamp)
				
				#un peu de mise en forme
				s_msg="$s_siteIpPingAgaintMsg ${a_sitesListName[$i_LoopCounter]} (${a_sitesListIp[i_LoopCounter]}) ($s_dateOfDownTime : $s_elapsedDownTime )"

				#et on envois
				sendToDiscordChanel "$s_discordWebhookKey" "$s_msg" "$s_discordBotName" "$s_discordBotAvatar"
				
				#Et on supprime le fichier http.memory, on en a plus besoin
				rm $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.ip.memory
				
			fi
		
		fi

		#et on efface l'evantuelle message
		s_msg=empty
		
		#On signal les problemes de ping sur l'addresse maintenant. Notez qu'on fait pas de différence entre une perte partielle ou totale.
		#Pour ça, il faut voir dans les logs eux-même
		if [ "${a_sitesAddressPingStatus[$i_LoopCounter]}" != "OK" ]
		then
		
			#Pas question de spammer tout les X minutes, ça va juste devenir illisible. 
			#On va utiliser le dossier p_memoryFolder pour savoir si l'annonce est deja faite
			test -f $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.address.memory || { echo -e "timestamp=$i_testTimeStamp" > $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.address.memory ; sendToDiscordChanel "$s_discordWebhookKey" "$s_siteIpLostAddressMsg ${a_sitesListName[$i_LoopCounter]} (${a_sitesListaddress[i_LoopCounter]})" "$s_discordBotName" "$s_discordBotAvatar" ;}
		
		else
		
			#tout va bien, mais es que c'etait le cas précédement ?		
			if test -f $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.address.memory
			then
			
				#un fichier http.memory est trouver, on sort donc dans un downtime
				#il y a une annonce à faire, pour ça on a besoin de quelques info				
				i_downTimestamp=$(f_ReadConfigLine $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.address.memory timestamp)
				i_elapsedDownTime=$(( $i_testTimeStamp - $i_downTimestamp))
				s_elapsedDownTime=$(convertAndPrintSeconds i_elapsedDownTime)
				s_dateOfDownTime=$(date -d @$i_downTimestamp)
				
				#un peu de mise en forme
				s_msg="$s_siteAddressPingAgaintMsg ${a_sitesListName[$i_LoopCounter]} (${a_sitesListaddress[i_LoopCounter]}) ($s_dateOfDownTime : $s_elapsedDownTime )"

				#et on envois
				sendToDiscordChanel "$s_discordWebhookKey" "$s_msg" "$s_discordBotName" "$s_discordBotAvatar"
				
				#Et on supprime le fichier http.memory, on en a plus besoin
				rm $p_memoryFolder/${a_sitesListName[$i_LoopCounter]}.ping.address.memory
				
			fi
		
		fi	
	
	
		#il faut incrementer le compteur
		i_LoopCounter=$((i_LoopCounter + 1))
		#et on efface l'evantuelle message
		s_msg=empty
		
	done

fi
